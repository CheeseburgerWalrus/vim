syntax on

set nu
set relativenumber
set incsearch
set hlsearch
set laststatus=2
set termguicolors
set mouse=a
set autochdir
set tabstop=2
set shiftwidth=2
set expandtab
"set complete+=kspell
"set completeopt=menuone,longest
"set shortmess+=c

" Plugins with VimPlug
call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'bluz71/vim-nightfly-guicolors'
Plug 'preservim/nerdtree'
Plug 'mattn/emmet-vim'
"Plug 'vim-scripts/AutoComplPop'
Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

" Nightfly colorscheme and lightline statusbar
colorscheme nightfly
let g:nightflyCursorColor = 1
let g:nightflyUnderlineMatchParen = 1

let g:lightline = {
      \ 'colorscheme': 'nightfly',
      \ 'active': {
      \   'right': [ [ 'lineinfo' ],
      \              [ 'percent' ],
      \              [ 'fileformat', 'fileencoding', 'filetype', 'charvaluehex' ] ]
      \ },
      \ 'component': {
      \   'charvaluehex': '0x%B'
      \ },
      \ }

" Command Mode Non Recursive
nnoremap <F12> :NERDTreeToggle<CR>
nnoremap ee $
nnoremap aa 0

" Insert Mode Non Recursive
inoremap qq <Esc>
inoremap ee <Esc>A
inoremap aa <Esc>0i
inoremap <C-h> <Left>
inoremap <C-l> <Right>
inoremap <C-k> <Up>
inoremap <C-j> <Down>

" Visual Mode Non Recursive
vnoremap qq <Esc> 

" Emmet
let g:user_emmet_expandabbr_key='<Tab><Tab>'
imap <expr> <tab><tab> emmet#expandAbbrIntelligent("\<tab><tab>")

" Splits
set splitbelow splitright

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap <silent> <C-Left> :vertical resize +2<CR>
nnoremap <silent> <C-Right> :vertical resize -2<CR>
nnoremap <silent> <C-Up> :vertical resize +2<CR>
nnoremap <silent> <C-Down> :vertical resize -2<CR>

" Terminal in vertical and horizontal splits
map <Leader>vv :vert term<CR>
map <Leader>hh :term<CR>

" Switch from vertical to horizontal split and the other way
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" to see changes made in an unsaved file
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
          \ | wincmd p | diffthis
endif

"Ale Related
let g:ale_linters = {'python':['flake8']}
let g:ale_fixers = {'*':['remove_trailing_lines', 'trim_whitespace'],'python':['black'], 'javascript':['eslint']}
let g:ale_fix_on_save = 1

nmap <F11> <Plug>(ale_fix)
imap <F11> <Esc><Plug>(ale_fix)

" Recommended/example setting from Coc.nvim page
set encoding=utf-8
set hidden
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c

" Using tab for code completion. I have assigned tab+tab for emmet
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
